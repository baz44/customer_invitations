(ns customer-invitations.core
  (:require [customer-invitations.parser :as parser]
            [customer-invitations.distance :as distance])
  (:gen-class))

(defn- generate-list [filename]
  ;; Parses a given customer file
  ;; and then generate a filtered customers list
  ;; that matches the criteria
  (->> (parser/parse-customer-file filename)
       (distance/filter-customer-list)))

(defn- print-list [customers-list]
  ;; Prints the customer list to STDOUT
  (->> customers-list
       (map #(select-keys % [:user_id :name]))
       (sort-by :user_id)
       (map  #(printf "ID: %d, Name: %s\n" (:user_id %) (:name %)))
       doall))

(defn -main [& args]
  "This is the entry point of the program"
  (-> (generate-list (first args))
      print-list)
  (flush))
