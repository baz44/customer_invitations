(ns customer-invitations.distance)

(def dublin-office-coordinates
  {:latitude  53.339428
   :longitude -6.257664})

(def earth-mean-radius 6371.0088)

(def max-distance 100)

(defn calculate-distance [latitude longitude]
  ;; Returns the greate-circle distance (KM) between
  ;; a given coordinates and dublin office
  (when (and latitude longitude)
    (* earth-mean-radius
       (Math/acos
        (+
         (* (Math/sin (Math/toRadians latitude))
            (Math/sin (Math/toRadians (:latitude dublin-office-coordinates))))

         (* (Math/cos (Math/toRadians latitude))
            (Math/cos (Math/toRadians (:latitude dublin-office-coordinates)))
            (Math/cos (-> (- (Math/toRadians longitude) (-> dublin-office-coordinates :longitude Math/toRadians))
                          Math/abs))))))))

(defn filter-customer-list [customers]
  ;; Returns list of customers that are within the 100KM distance
  ;; from the dublin office
  (->> customers
       (filter (fn [customer]
                 (when-let [distance (calculate-distance (some-> customer :latitude Float/parseFloat)
                                                         (some-> customer :longitude Float/parseFloat))]
                   (<= distance max-distance))))))
