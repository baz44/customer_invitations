(ns customer-invitations.parser
  (:require [cheshire.core :refer [parsed-seq]]))

(defn parse-customer-file [file]
  (try
    (parsed-seq (clojure.java.io/reader file) true)

    (catch java.io.FileNotFoundException e
      (printf "The file: %s you specified does not exist.\n" file))

    (catch Exception e
      (println "Please provide a valid cutomer list file."))))
