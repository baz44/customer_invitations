(ns customer-invitations.distance-test
  (:require [customer-invitations.distance :as distance]
            [clojure.test :refer :all]))


(deftest calculate-distance-test
  (testing "it should return the distance from the dublin office for a given location"
    (is (= (distance/calculate-distance (:latitude distance/dublin-office-coordinates)
                                        (:longitude distance/dublin-office-coordinates)) 0.0))

    (is (= (distance/calculate-distance 52.986375 -6.043701) 41.76878319430922)))


  (testing "it should return nil if no latitude or longitude is given"
    (is (nil? (distance/calculate-distance 52.986375 nil)))
    (is (nil? (distance/calculate-distance nil -6.043701)))
    (is (nil? (distance/calculate-distance nil nil)))))


(deftest filter-customer-list-test
  (testing "it should return a list of customers that are withing 100km from the dublin office"
    (let [customers [{:latitude "52.986375"  :user_id 12 :name "Christina McArdle" :longitude "-6.043701"}
                     {:latitude "51.92893"   :user_id 1  :name "Alice Cahill"      :longitude "-10.27699"}
                     {:latitude "51.8856167" :user_id 2  :name "Ian McArdle"       :longitude "-10.4240951"}
                     {:latitude "53.2451022" :user_id 4  :name "Ian Kehoe"         :longitude "-6.238335"}]]

      (is (= [{:latitude "52.986375"  :user_id 12 :name "Christina McArdle" :longitude "-6.043701"}
              {:latitude "53.2451022" :user_id 4  :name "Ian Kehoe"         :longitude "-6.238335"}]
             (distance/filter-customer-list customers)))))

  (testing "it should ignore any customer that's missing latitude or longitude"
            (is (= [{:latitude "53.2451022" :user_id 4  :name "Ian Kehoe" :longitude "-6.238335"}]
             (distance/filter-customer-list [{:latitude "52.986375"  :user_id 12 :name "Christina McArdle"}
                                             {:latitude "51.92893"   :user_id 1  :name "Alice Cahill"      :longitude "-10.27699"}
                                             {:latitude "51.8856167" :user_id 2  :name "Ian McArdle"       :longitude "-10.4240951"}
                                             {:latitude "53.2451022" :user_id 4  :name "Ian Kehoe"         :longitude "-6.238335"}])))

      (is (= [{:latitude "53.2451022" :user_id 4  :name "Ian Kehoe" :longitude "-6.238335"}]
             (distance/filter-customer-list [{:user_id 12 :name "Christina McArdle" :longitude "-6.043701"}
                                             {:latitude "51.92893"   :user_id 1  :name "Alice Cahill"      :longitude "-10.27699"}
                                             {:latitude "51.8856167" :user_id 2  :name "Ian McArdle"       :longitude "-10.4240951"}
                                             {:latitude "53.2451022" :user_id 4  :name "Ian Kehoe"         :longitude "-6.238335"}])))

      (is (= [{:latitude "53.2451022" :user_id 4  :name "Ian Kehoe" :longitude "-6.238335"}]
             (distance/filter-customer-list [{:user_id 12 :name "Christina McArdle"}
                                             {:latitude "51.92893"   :user_id 1  :name "Alice Cahill"      :longitude "-10.27699"}
                                             {:latitude "51.8856167" :user_id 2  :name "Ian McArdle"       :longitude "-10.4240951"}
                                             {:latitude "53.2451022" :user_id 4  :name "Ian Kehoe"         :longitude "-6.238335"}])))))
