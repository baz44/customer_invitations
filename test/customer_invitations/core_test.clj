(ns customer-invitations.core-test
  (:require [clojure.test :refer :all]
            [customer-invitations.core :refer :all]))

(deftest main-test
  (testing "it should print a list of customers within 100km from the dublin office sorted by user_id asc"
    (is (= "ID: 4, Name: Ian Kehoe\nID: 5, Name: Nora Dempsey\nID: 6, Name: Theresa Enright\nID: 8, Name: Eoin Ahearn\nID: 11, Name: Richard Finnegan\nID: 12, Name: Christina McArdle\nID: 13, Name: Olive Ahearn\nID: 15, Name: Michael Ahearn\nID: 17, Name: Patricia Cahill\nID: 23, Name: Eoin Gallagher\nID: 24, Name: Rose Enright\nID: 26, Name: Stephen McArdle\nID: 29, Name: Oliver Ahearn\nID: 30, Name: Nick Enright\nID: 31, Name: Alan Behan\nID: 39, Name: Lisa Ahearn\n"
           (with-out-str (-main "test_resources/customer_list.json"))))))
