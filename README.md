# customer_invitations

A Clojure program to return a list of customers that are within 100KM from intercom dublin office.

## Compiling
- Install lein (https://leiningen.org).
- Run ``lein uberjar``
- This will generate a jar file ```target/customer_invitations-0.1.0-SNAPSHOT-standalone.jar```
- After compiling the program you can execute the program buy running the jar file:

	```java -jar target/customer_invitations-0.1.0-SNAPSHOT-standalone.jar customer_list.json```

	where customer_list.json is the path to the customers list json file.

## Run from compiled file
I have included a compiled version of the program, to run:

```java -jar customer_invitations-0.1.0-SNAPSHOT-standalone.jar customer_list.json```

where customer_list.json is the path to the customers list json file.

## Tests
You will need lein installed (Note the compliling step), You can run the tests by:

```lein test```

## Output

I have included the output of the execution of the program for the given customer list in output.txt file.

## License

Copyright © 2020 Basel Farah

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
